import { Request, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

export class MockXHRBackend {
  constructor() {
  }

  createConnection(request: Request) {
    var response = new Observable((responseObserver: Observer<Response>) => {
      var responseOptions;
      switch (request.method) {
        case RequestMethod.Get:
          if (request.url.indexOf('favdealeritems') >= 0) {
           
            var favdealerItems;
            favdealerItems = this._dealerItems.filter(favdealerItem => favdealerItem.isFavorite === true);
            responseOptions = new ResponseOptions({
              body: { favdealerItems: JSON.parse(JSON.stringify(favdealerItems)) },
              status: 200
            });
          }
          else if (request.url.indexOf('editDealer?id=') >= 0) {
            console.log("editdealer id");
           
            var id = parseInt(request.url.split('=')[1]);
            dealerItems = this._dealerItems.filter(dealerItem => dealerItem.id === id);
            responseOptions = new ResponseOptions({
              body: {dealerItem : JSON.parse(JSON.stringify(dealerItems[0]))},
              status: 200
            });
          }
          else if (request.url.indexOf('dealeritems?medium=') >= 0 || request.url === 'dealeritems') {
            var medium;
            if (request.url.indexOf('?') >= 0) {
              medium = request.url.split('=')[1];
              if (medium === 'undefined') medium = '';
            }
            var dealerItems;
            if (medium) {
              if(medium == "favorite"){
              dealerItems = this._dealerItems.filter(dealerItem => dealerItem.isFavorite === true);
              }else{
                 dealerItems = this._dealerItems.filter(dealerItem => dealerItem.medium === medium);
              }
             
            } else {
              dealerItems = this._dealerItems;
            }
            responseOptions = new ResponseOptions({
              body: { dealerItems: JSON.parse(JSON.stringify(dealerItems)) },
              status: 200
            });
          } else {
            console.log("unexpected route",request.url);
            var id = parseInt(request.url.split('/')[1]);
            dealerItems = this._dealerItems.filter(dealerItem => dealerItem.id === id);
            responseOptions = new ResponseOptions({
              body: JSON.parse(JSON.stringify(dealerItems[0])),
              status: 200
            });
          }
          break;
        case RequestMethod.Post:
          var dealerItem = JSON.parse(request.text().toString());
          dealerItem.id = this._getNewId();
          dealerItem.isfavorite= false;
          this._dealerItems.push(dealerItem);
          responseOptions = new ResponseOptions({ status: 201 });
          break;
        case RequestMethod.Delete:
          var id = parseInt(request.url.split('/')[1]);
          this._deleteDealerItem(id);
          responseOptions = new ResponseOptions({ status: 200 });
          break;
        case RequestMethod.Put:
          var updateItem = JSON.parse(request.text().toString());
          if (request.url.indexOf('editdealer') >= 0) {
          console.log("update",updateItem);
          this._editDealer(updateItem);
          responseOptions = new ResponseOptions({ status: 200 });


          }
          else if(request.url.indexOf('dealeritems') >= 0){
          this._favDealerItem(updateItem.id);
          responseOptions = new ResponseOptions({ status: 200 });
          }else{
          responseOptions = new ResponseOptions({ status: 404 });
          }
          
            
      }

      var responseObject = new Response(responseOptions);
      responseObserver.next(responseObject);
      responseObserver.complete();
      return () => { };
    });
    return { response };
  }

  _favDealerItem(id) {
    var dealerItem = this._dealerItems.find(dealerItem => dealerItem.id === id);
    var index = this._dealerItems.indexOf(dealerItem);
    if (index >= 0) {
      this._dealerItems[index].isFavorite = !this._dealerItems[index].isFavorite;
          }
  }

  _editDealer(updateItem) {
    console.log("updateid",updateItem.id);
    var dealerItem = this._dealerItems.find(dealerItem => dealerItem.id == updateItem.id);
    var index = this._dealerItems.indexOf(dealerItem);
    console.log("index",index);
    if (index >= 0) {
      console.log(this._dealerItems[index]);
      console.log(updateItem);
      this._dealerItems[index] = updateItem;
          }
  }

  _deleteDealerItem(id) {
    var dealerItem = this._dealerItems.find(dealerItem => dealerItem.id === id);
    var index = this._dealerItems.indexOf(dealerItem);
    if (index >= 0) {
      this._dealerItems.splice(index, 1);
    }
  }

  _getNewId() {
    if (this._dealerItems.length > 0) {
      return Math.max.apply(Math, this._dealerItems.map(dealerItem => dealerItem.id)) + 1;
    } else {
      return 1;
    }
  }

  _dealerItems = [
    {
      id: 1,
      name: "Haseeb",
      medium: "Boats",
      category: "Mercury",
      year: 2010,
      lat:"32.7833333",
      lng:"-96.8",
      isFavorite: false
    },
    {
      id: 2,
      name: "Omer",
      medium: "Boats",
      category: "Searay",
      year: 2015,
      lat:"30.26715",
      lng:"-97.74306",
      isFavorite: true
    }, {
      id: 3,
      name: "Arshad",
      medium: "Boats",
      category: "Regmarine",
      year: 2016,
      lat:"29.76328",
      lng:"-95.36327",
      isFavorite: false
    }, {
      id: 4,
      name: "Zakir",
      medium: "Auto",
      category: "Toyota",
      year: null,
      lat:"29.42412",
      lng:"-98.49363",
      isFavorite: true
    }, {
      id: 5,
      name: "Salman",
      medium: "Auto",
      category: "Ford",
      year: 2015,
      lat:"33.019844",
      lng:"-96.698883",
      isFavorite: false
    }
  ];
}
