import { Component, Inject } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { DealerItemService } from './dealer-item.service';
import { lookupListToken } from './providers';

@Component({
  selector: 'edit-form',
  templateUrl: 'app/update-dealer.component.html',
  styleUrls: ['app/update-dealer.component.css']
})
export class UpdateDealer {
  form;
  dealerItem = {};
  medium =" ";
  id= 0;
  paramsSubscription;

  constructor(
    private formBuilder: FormBuilder,
    private dealerItemService: DealerItemService,
    private activatedRoute: ActivatedRoute,
    @Inject(lookupListToken) public lookupLists,
    private router: Router) {}

  ngOnInit() {
    this.paramsSubscription = this.activatedRoute.params
      .subscribe(params => {
        let id = params['id'];
        this.getDealer(id);
        
      });
      
    
  }

  getDealer(id) {
    console.log("id is-------",id);
    this.dealerItemService.getEdit(id)
      .subscribe(dealerItem => {
          this.dealerItem = dealerItem;
          this.id = id;
          console.log(dealerItem.category);
          this.form = this.formBuilder.group({
            medium: this.formBuilder.control(dealerItem.medium + ''),
            name: this.formBuilder.control(dealerItem.name + '', Validators.compose([
            Validators.required,
            Validators.pattern('[\\w\\-\\s\\/]+')
            ])),
            category: this.formBuilder.control(dealerItem.category + ''),
            lat:  this.formBuilder.control(dealerItem.lat + ''),
            lng:  this.formBuilder.control(dealerItem.lng + ''),
            year: this.formBuilder.control(dealerItem.year + '', this.yearValidator),
          });
          
          
      });
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  yearValidator(control) {
    if (control.value.trim().length === 0) {
      return null;
    }
    let year = parseInt(control.value);
    let minYear = 1800;
    let maxYear = 2500;
    if (year >= minYear && year <= maxYear) {
      return null;
    } else {
      return {
        'year': {
          min: minYear,
          max: maxYear
        }
      };
    }
  }

  onSubmit(dealerItem) {
    dealerItem.id = this.id;
    this.dealerItemService.update(dealerItem)
      .subscribe(() => {
        this.router.navigate(['/', dealerItem.medium]);
      });
  }
}
