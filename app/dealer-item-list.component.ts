import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { DealerItemService } from './dealer-item.service';

@Component({
  selector: 'mw-media-item-list',
  templateUrl: 'app/dealer-item-list.component.html',
  styleUrls: ['app/dealer-item-list.component.css']
})
export class DealerItemListComponent {
  medium = '';
  dealerItems = [];
  paramsSubscription;

  constructor(
    private dealerItemService: DealerItemService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.paramsSubscription = this.activatedRoute.params
      .subscribe(params => {
        let medium = params['mediums'];
        if(medium.toLowerCase() === 'all') {
          medium = '';
        }
        this.getDealerItems(medium);
      });
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

  onDealerItemDelete(dealerItem) {
    this.dealerItemService.delete(dealerItem)
      .subscribe(() => {
        this.getDealerItems(this.medium);
      });
  }

  onDealerItemEdit(dealerItem) {
  this.router.navigate(['/edit', dealerItem.id]);
    
  }

  onDealerItemFav(dealerItem) {
    this.dealerItemService.updatefav(dealerItem)
      .subscribe(() => {
        this.getDealerItems(this.medium);
      });
  }

  getDealerItems(medium) {
    this.medium = medium;
    this.dealerItemService.get(medium)
      .subscribe(dealerItems => {
        this.dealerItems = dealerItems;
      });
  }
}
