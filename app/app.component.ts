import { Component } from '@angular/core';

@Component({
  selector: 'dealerapp-app',
  templateUrl: 'app/app.component.html',
  styleUrls: ['app/app.component.css']
})
export class AppComponent { }
