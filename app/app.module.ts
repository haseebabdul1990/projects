import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule, XHRBackend } from '@angular/http';

import { AppComponent } from './app.component';
import { DealerItemComponent } from './dealer-item.component';
import { DealerItemListComponent } from './dealer-item-list.component';
import { FavDealerListComponent } from './dealer-fav-list.component';
import { FavoriteDirective } from './favorite.directive';
import { CategoryListPipe } from './category-list.pipe';
import { DealerItemFormComponent } from './dealer-item-form.component';
import { UpdateDealer } from './update-dealer.component';
import { DealerItemService } from './dealer-item.service';
import { lookupListToken, lookupLists } from './providers';
import { MockXHRBackend } from './mock-xhr-backend';
import { routing } from './app.routing';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    AppComponent,
    DealerItemComponent,
    DealerItemListComponent,
    UpdateDealer,
    FavDealerListComponent,
    FavoriteDirective,
    CategoryListPipe,
    DealerItemFormComponent
  ],
  providers: [
    DealerItemService,
    { provide: lookupListToken, useValue: lookupLists },
    { provide: XHRBackend, useClass: MockXHRBackend }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}