import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DealerItemService } from './dealer-item.service';

@Component({
  selector: 'fav-item-list',
  templateUrl: 'app/dealer-fav-list.component.html',
})
export class FavDealerListComponent {
  favdealerItems = [];

  constructor(
    private dealerItemService: DealerItemService,
    private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
   this.getDealerfavs();
     
  }

  ngOnDestroy() {
    
  }


  getDealerfavs() {
    this.dealerItemService.getfav()
      .subscribe(favdealerItems => {
        this.favdealerItems = favdealerItems;
      });
  }
}
