import { Routes, RouterModule } from '@angular/router';

import { DealerItemFormComponent } from './dealer-item-form.component';
import { FavDealerListComponent } from './dealer-fav-list.component';
import { DealerItemListComponent } from './dealer-item-list.component';
import { UpdateDealer } from './update-dealer.component';
const appRoutes: Routes = [
  { path: 'add', component: DealerItemFormComponent },
  { path: 'edit/:id', component: UpdateDealer },
  { path: ':mediums', component: DealerItemListComponent },
  { path: 'fav', component: FavDealerListComponent },
  { path: '', pathMatch: 'full', redirectTo: 'all' }
];

export const routing = RouterModule.forRoot(appRoutes);
