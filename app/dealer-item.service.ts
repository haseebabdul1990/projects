import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DealerItemService {
  constructor(private http: Http) {}

  get(medium) {
    let searchParams = new URLSearchParams();
    searchParams.append('medium', medium);
    return this.http.get('dealeritems', { search: searchParams })
      .map(response => {
        return response.json().dealerItems;
      });
  }

   getEdit(id) {
    let searchParams = new URLSearchParams();
    searchParams.append('id', id);
    return this.http.get('editDealer', { search: searchParams })
      .map(response => {
        return response.json().dealerItem;
      });
  }

  getfav(){
   return this.http.get('favdealeritems')
      .map(response => {
        return response.json().favdealerItems;
      });
  }

  updatefav(dealerItem) {
  return this.http.put('dealeritems', dealerItem)
      .map(response => {});
  }

  update(dealerItem) {
  return this.http.put('editdealer', dealerItem)
      .map(response => {});
  }

  
  add(dealerItem) {
    return this.http.post('dealeritems', dealerItem)
      .map(response => {});
  }
  
  delete(dealerItem) {
    return this.http.delete(`dealeritems/${dealerItem.id}`)
      .map(response => {});
  }
}
