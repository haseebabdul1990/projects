import { Pipe } from '@angular/core';

@Pipe({
  name: 'categoryList'
})
export class CategoryListPipe {
  transform(dealerItems) {
    var categories = [];
    dealerItems.forEach(dealerItem => {
      if (categories.indexOf(dealerItem.category) <= -1) {
        categories.push(dealerItem.category);
      }
    });
    return categories.join(', ');
  }
}