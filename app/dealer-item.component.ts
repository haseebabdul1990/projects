import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'dealerapp-dealer-item',
  templateUrl: 'app/dealer-item.component.html',
  styleUrls: ['app/dealer-item.component.css']
})
export class DealerItemComponent {
  @Input() dealerItem;
  @Output() delete = new EventEmitter();
  @Output() favorite = new EventEmitter();
  @Output() edit = new EventEmitter();

  onDelete() {
    this.delete.emit(this.dealerItem);
  }
  onFav()	{
  this.favorite.emit(this.dealerItem);
  }

  onEdit() {
    this.edit.emit(this.dealerItem);
  }
}
